<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__ . '/../init.php';

check_login();

$action = !empty($_GET['action']) ? $_GET['action'] : 'list';

if ($action == 'list') {
    $page_param = 'page';
    $page_size = 10;
    $current_page = !empty($_GET[$page_param]) ? (int)$_GET[$page_param] : 1;

    if (is_current_login_user_super_admin()) {
        $dept_id = null;
    } else {
        $dept_id = get_current_login_user_info('dept_id');
    }
    $data = get_notes_pagination($current_page, $page_param, $page_size, $dept_id, !empty($_GET['only-not-replied']));

    echo renderWithAdminLayout('admin/reply', $data);
} else if ($action == 'delete') {
    $id = $_GET['id'];
    $note = get_note_by_id($id);
    if (is_current_login_user_can_handle($note['dept_id'])) {
        delete_note($id);
        set_success_message('删除成功');
    } else {
        set_error_message('你没有权限删除该条留言');
    }
    redirect_path('admin/reply.php');
} else if ($action == 'reply') {
    if (!empty($_POST['reply_note_id']) && !empty($_POST['content'])) {
        add_note_reply($_POST['reply_note_id'], $_POST['content'], get_current_login_user_info('id'));
        echo_json_success('回复成功');
    } else {
        echo_json_error('请填写完整数据');
    }
}