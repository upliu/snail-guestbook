<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__.'/../init.php';

if (empty($_POST)) {
    echo renderWithLayout('admin/login');
    exit();
}

if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $return = admin_user_login($_POST['username'], $_POST['password']);
    if (is_success($return)) {
        redirect_admin_home();
    } else {
        $message = $return['message'];
    }
} else {
    $message = '用户名和密码不能为空';
}

set_error_message($message);
echo renderWithLayout('admin/login');