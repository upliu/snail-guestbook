<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__ . '/../init.php';

check_login();

if (empty($_POST)) {
    echo renderWithAdminLayout('admin/my');
    exit();
}

if (!empty($_POST['old_password']) && !empty($_POST['new_password']) && !empty($_POST['re_new_password']))
{
    if ($_POST['new_password'] == $_POST['re_new_password']) {
        $user = get_current_login_user_info();
        if (is_password_pass($user['salt'], $user['password'], $_POST['old_password'])) {
            change_admin_user_password($user['id'], $_POST['new_password']);
            set_success_message('修改密码成功，请重新登录');
            redirect_path('admin/logout.php');
        } else {
            $message = '原密码错误';
        }
    } else {
        $message = '两次输入新密码不一致';
    }
} else {
    $message = '请输入完整数据';
}

set_error_message($message);
echo renderWithAdminLayout('admin/my');