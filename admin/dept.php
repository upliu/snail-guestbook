<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__ . '/../init.php';

check_login();

if (!is_current_login_user_super_admin()) {
    die('Pemission denied!');
}

$action = !empty($_GET['action']) ? $_GET['action'] : 'list';

switch ($action) {
    case 'list':
        $depts = get_dept_list();
        echo renderWithAdminLayout('admin/dept', array(
            'depts' => $depts,
        ));
        exit();
        break;
    case 'set-is-show':
        set_dept_is_show($_GET['id'], $_GET['is-show']);
        set_success_message('设置成功');
        break;
    case 'delete':
        delete_dept_by_id($_GET['id']);
        set_success_message('删除成功');
        break;
    case 'add':
        if (!empty($_POST['name'])) {
            try {
                add_dept($_POST['name']);
                set_success_message('部门添加成功');
            } catch (\Exception $e) {
                set_error_message($e->getMessage());
            }
        } else {
            set_error_message('部门名称不能为空');
        }
        break;
    case 'update':
        break;
}

redirect_path('admin/dept.php');