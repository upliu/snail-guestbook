<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__ . '/init.php';

if (empty($_POST)) {
    echo renderWithLayout('write');
    exit();
}

if (empty($_POST['nickname'])) {
    $message = '昵称不能为空';
} else if (empty($_POST['content'])) {
    $message = '留言内容不能为空';
} else {
    try {
        add_note(array(
            'nickname' => $_POST['nickname'],
            'qq' => !empty($_POST['qq']) ? $_POST['qq'] : '',
            'email' => !empty($_POST['email']) ? $_POST['email'] : '',
            'content' => $_POST['content'],
            'dept_id' => !empty($_POST['dept_id']) ? (int)$_POST['dept_id'] : 0,
        ));
        set_success_message('留言成功');
        redirect_home();
    } catch (\Exception $e) {
        $message = '插入数据库出错：' . $e->getMessage();
    }
}

set_error_message($message);
echo renderWithLayout('write');