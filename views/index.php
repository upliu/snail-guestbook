<p>
    <a href="write.php" class="btn">发表留言</a>
</p>
<div class="notes">
    <?php foreach ($notes as $note) { ?>
        <div class="note">
            <div class="meta"><?php e($note['nickname']); ?>[ip:<?php echo long2ip($note['ip']) ?>] 发表于：<?php echo $note['create_time']; ?></div>
            <div class="content">
                <p><?php e($note['content']); ?></p>
                <?php if (!empty($replys[$note['id']])) { foreach ($replys[$note['id']] as $reply) { ?>
                    <div class="reply">
                        <div><?php e(get_dept_name_by_id($reply['dept_id'])) ?>回复：</div>
                        <div><?php e($reply['content']); ?></div>
                    </div>
                <?php }} ?>
            </div>
        </div>
    <?php } ?>
</div>

<?php echo $pagination; ?>