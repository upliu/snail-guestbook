<form method="get" class="action-area">
    只查询未回复的留言？<input type="checkbox" name="only-not-replied"
                     value="1" <?php if ($_GET['only-not-replied']) echo 'checked'; ?>>
    <input type="submit" value="确定">
</form>
<div class="notes">
    <?php foreach ($notes as $note) { ?>
        <div class="note" data-id="<?php echo $note['id']; ?>">
            <div class="meta">
                <?php e($note['nickname']); ?>[ip:<?php echo long2ip($note['ip']) ?>]
                发表于：<?php echo $note['create_time']; ?>
                <?php if ($note['email'] || $note['qq']) { echo '<br>'; } ?>
                <?php e_if_true($note['email'], '邮箱：%s') ?>
                <?php e_if_true($note['qq'], 'QQ：%s') ?>
                <span class="note-action">
                    <?php if (!$note['is_replied']) { ?>
                        <a class="cmd-reply" href="javascript:;">回复</a>
                    <?php } ?>
                    <a data-confirm="确定删除该留言？" href="?action=delete&id=<?php echo $note['id']; ?>">删除</a>
                </span>
            </div>
            <div class="content">
                <p><?php e($note['content']); ?></p>
                <?php if (!empty($replys[$note['id']])) {
                    foreach ($replys[$note['id']] as $reply) { ?>
                        <div class="reply">
                            <div><?php e(get_dept_name_by_id($reply['dept_id'])) ?>回复：</div>
                            <div><?php e($reply['content']); ?></div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    <?php } ?>
</div>

<?php echo $pagination; ?>

<div id="reply-box" style="display: none">
    <textarea name="content" id="" cols="70" rows="5"></textarea>

    <div>
        <a href="javascript:;" class="btn cmd-do-reply">回复</a>
        <a href="javascript:;" class="btn cmd-do-reply-cancel">取消</a>
    </div>
</div>