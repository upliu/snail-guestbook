<div class="nav-wrap">
    <ul class="nav">
        <li class="<?php echo_active_if_true('index.php') ?>"><a class="btn" href="index.php">首页</a></li>
        <li class="<?php echo_active_if_true('reply.php') ?>"><a class="btn" href="reply.php">管理留言</a></li>
        <?php if (is_current_login_user_super_admin()) { ?>
        <li class="<?php echo_active_if_true('user.php') ?>"><a class="btn" href="user.php">管理用户</a></li>
        <li class="<?php echo_active_if_true('dept.php') ?>"><a class="btn" href="dept.php">管理部门</a></li>
        <?php } ?>
        <li class="<?php echo_active_if_true('my.php') ?>"><a class="btn" href="my.php">修改密码</a></li>
        <li class="<?php echo_active_if_true('logout.php') ?>"><a class="btn" href="logout.php">退出（<?php e(get_current_login_user_info('username')) ?>）</a></li>
    </ul>
</div>

<?php echo $__content; ?>