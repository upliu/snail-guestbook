<div class="action-area" style="text-align: right">
    <a href="?action=add" class="btn">添加新用户</a>
</div>
<table class="users">
    <tr>
        <th>ID</th>
        <th>用户名</th>
        <th>部门</th>
        <th>帐号状态</th>
        <th>操作</th>
    </tr>
    <?php foreach ($users as $user) { ?>
        <tr>
            <td><?php echo $user['id']; ?></td>
            <td><?php e($user['username']); ?></td>
            <td><?php e(get_dept_name_by_id($user['dept_id'])); ?></td>
            <td><?php e(get_admin_user_status_desc($user['status'])) ?></td>
            <td>
                <?php if ($user['status'] == ADMIN_USER_STATUS_ACTIVE) { ?>
                    <a href="?action=set-forbidden&user_id=<?php echo $user['id']; ?>">禁用</a>
                <?php } else { ?>
                    <a href="?action=set-active&user_id=<?php echo $user['id']; ?>">启用</a>
                <?php } ?>
                <a href="?action=update&user_id=<?php echo $user['id']; ?>">编辑</a>
                <a data-confirm="确定删除用户吗？" href="?action=delete&user_id=<?php echo $user['id']; ?>">删除</a>
            </td>
        </tr>
    <?php } ?>
</table>