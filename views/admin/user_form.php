<form method="post">
    <table>
        <tr>
            <td class="label">用户名</td>
            <td><input type="text" name="username" value="<?php e($user['username']); ?>" <?php if ($is_update_user) { echo 'disabled'; } ?>></td>
        </tr>
        <tr>
            <td class="label">密码：</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td class="label">所属部门：</td>
            <td><?php echo html_select_render(get_depts(), '管理员', array('name' => 'dept_id', 'selected' => $user['dept_id'])); ?></td>
        </tr>
    </table>
    <div>
        <input type="submit" class="btn" value="提交">
    </div>
</form>