<form action="?action=add" method="post" class="action-area">
    <input type="text" name="name" style="width: 80px" required>
    <input type="submit" class="btn" value="添加新部门">
</form>
<table class="users">
    <tr>
        <th>ID</th>
        <th>部门</th>
        <th>是否前台显示</th>
        <th>操作</th>
    </tr>
    <?php foreach ($depts as $dept) { ?>
        <tr>
            <td><?php echo $dept['id']; ?></td>
            <td><?php e($dept['name']); ?></td>
            <td><?php echo $dept['is_show'] ? '是' : '否' ?></td>
            <td>
                <?php if ($dept['is_show']) { ?>
                    <a href="?action=set-is-show&id=<?php echo $dept['id']; ?>&is-show=0">不显示</a>
                <?php } else { ?>
                    <a href="?action=set-is-show&id=<?php echo $dept['id']; ?>&is-show=1">显示</a>
                <?php } ?>
                <a data-confirm="确定删除该部门吗？" href="?action=delete&id=<?php echo $dept['id']; ?>">删除</a>
            </td>
        </tr>
    <?php } ?>
</table>