<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>小蜗牛留言本</title>
    <link href="<?php static_url('style.css') ?>" rel="stylesheet">
</head>
<body>
<iframe src="https://demo.upliu.net/" frameborder="0" width="100%"></iframe>
<div id="tip-error" class="tip error" style="display: <?php echo is_has_error_message() ? 'block' : 'none' ?>"><?php echo get_error_message(); ?></div>

<div id="tip-success" class="tip success" style="display: <?php echo is_has_success_message() ? 'block' : 'none' ?>"><?php echo get_success_message(); ?></div>

<h1 class="title">小蜗牛留言本</h1>

<div class="container">
    <?php echo $__content; ?>
</div>

<div id="footer">
    &copy;<?php echo date('Y'); ?>
    Powerd by <a href="http://demo.upliu.net/snail-guestbook/" target="_blank">小蜗牛留言本</a>
    <a href="<?php echo get_project_url().'/admin/'; ?>">后台管理</a>
</div>
<script src="<?php static_url('jquery.min.js') ?>"></script>
<script src="<?php static_url('script.js') ?>"></script>
</body>
</html>