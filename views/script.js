var Snail = {};
Snail.success = function (msg) {
    $('#tip-success').text(msg).css('display', 'block');
    Snail.tipFadeOut();
};
Snail.error = function (msg) {
    $('#tip-error').text(msg).css('display', 'block');
    Snail.tipFadeOut();
};
Snail.tipFadeOut = function () {
    setTimeout(function () {
        $('.tip').fadeOut();
    }, 2000);
};

$(function () {

    Snail.tipFadeOut();

    $('a[data-confirm]').click(function () {
        var prompt = $(this).data('confirm');
        if (!confirm(prompt)) {
            return false;
        }
    });

    $('.cmd-reply').click(function () {
        window._reply_note_id = $(this).closest('.note').data('id');
        $('#reply-box').appendTo($(this).closest('.note').find('.content')).css('display', 'block');
    });

    $('body')
        .on('click', '.cmd-do-reply', function () {
            var content = $('#reply-box').find('[name="content"]').val();
            if (!content) {
                Snail.error('回复内容不能为空');
                return false;
            }
            $.post('reply.php?action=reply', {
                reply_note_id: window._reply_note_id,
                content: content
            }, function (data) {
                if (data.status == 0) {
                    Snail.success(data.message);
                    window.location.reload();
                } else {
                    Snail.error(data.message);
                }
            }, 'json');
        })
        .on('click', '.cmd-do-reply-cancel', function () {
            $('#reply-box').css('display', 'none');
        });
});