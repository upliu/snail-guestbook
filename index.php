<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

require __DIR__ . '/init.php';

if (!is_installed()) {
    redirect_path('install.php');
}

$page_param = 'page';
$page_size = 10;
$current_page = !empty($_GET[$page_param]) ? (int)$_GET[$page_param] : 1;

$data = get_notes_pagination($current_page, $page_param, $page_size);

echo renderWithLayout('index', $data);