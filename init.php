<?php
/**
 * 小蜗牛留言本
 * 本文件内代码属于“小蜗牛留言本”项目，禁止修改再发布本程序源码，禁止去除页面底部的版权申明。所有版权保留
 * User: upliu
 * Email: 753073861@qq.com
 * QQ: 753073861
 * 程序主页：http://demo.upliu.net/snail-guestbook/
 */

error_reporting(E_ALL);
date_default_timezone_set('Asia/Shanghai');
session_start();

define('DIR_VIEWS', __DIR__ . '/views');

require __DIR__ . '/include/library.php';
require __DIR__ . '/include/model.php';

